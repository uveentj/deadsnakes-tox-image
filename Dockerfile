FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install --no-install-recommends -y software-properties-common gnupg python3-pip && \
    add-apt-repository -y ppa:deadsnakes/ppa && \
    apt-get update

RUN apt-get install -y python3.7 python3.7-distutils \
                       python3.8 python3.8-distutils \
                       python3.9 python3.9-distutils \
                       python3.10 python3.10-distutils \
                       python3.11 python3.11-distutils

