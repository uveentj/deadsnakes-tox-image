# What's this

A docker image with different python versions (3.7 to 3.11 as of now) preinstalled
from the [deadsnackes ppa](https://launchpad.net/~deadsnakes/+archive/ubuntu/ppa)
for testing with [tox](https://tox.wiki/en/4.6.4/) on different python versions.

# How to use

Add a job to your `.gitlab-ci.yml`

```yaml
toxrun:
  stage: test
  image: docker.gitlab.gwdg.de/uveentj/deadsnakes-tox-image:latest
  before_script:
    - pip install tox
  script:
    - tox
```

and a tox.ini (or a config entry in setup.cfg / pyproject.toml)

```ini
[tox]
env_list = py{37,38,39,310,311}
```

